import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Utility {

    public static String webSiteUrl="http://www.n11.com";
    public static WebDriver driver;
    public static String FavoriteProduct;




    @BeforeClass
    public static void setup() {
        System.out.println("***  Web Sitesi Yükleniyor... ***");
        System.out.println(" >>> \n");
         System.setProperty("webdriver.gecko.driver", "C:\\Users\\senih\\Desktop\\Projeler\\Selenium\\geckodriver.exe");
       // System.setProperty("webdriver.gecko.driver", "Driver Yolu");
        driver = new FirefoxDriver();
        driver.get(webSiteUrl);

    }

    @AfterClass
    public static void closeCase() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();
    }
}
