import com.google.common.base.Function;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/*
* 1. http://www.n11.com <http://www.n11.com/> sitesine gelecek ve anasayfanin acildigini onaylayacak

2. Login ekranini acip, bir kullanici ile login olacak ( daha once siteye uyeligin varsa o olabilir )

3. Ekranin ustundeki Search alanina 'samsung' yazip Ara butonuna tiklayacak

4. Gelen sayfada samsung icin sonuc bulundugunu onaylayacak

5. Arama sonuclarindan 2. sayfaya tiklayacak ve acilan sayfada 2. sayfanin su an gosterimde oldugunu onaylayacak

6. Ustten 3. urunun icindeki 'favorilere ekle' butonuna tiklayacak

7. Ekranin en ustundeki 'favorilerim' linkine tiklayacak

8. Acilan sayfada bir onceki sayfada izlemeye alinmis urunun bulundugunu onaylayacak

9. Favorilere alinan bu urunun yanindaki 'Kaldir' butonuna basarak, favorilerimden cikaracak

10. Sayfada bu urunun artik favorilere alinmadigini onaylayacak.*/
public class TestPage extends Page{

    WebDriverWait wait= new WebDriverWait(driver, 1000);

    //Tüm testlerin kontrolü
    @Test
    public void AllTest_0(){
        //Anasayfa açılış kontrolü
        test_1_WebSiteControl();
        //Kullanıcı giriş sayfası açılma kontrolü
        test_2_loginPageReadyControl();
        //Kullanıcı girişi kontrolü
        test_3_loginControl();
        //samsung için arama sonucu kontrolü
        test_4_searchControl();
        //ikinci sayfaya tıklanma ve 2. sayfanın açılma kontrolü
        test_5_clickPageAndPageTwoOpenedControl();
        //listede 3. ürünün favorilere eklenme kontrolü
        test_6_addThirdProductFavorite();
        //Favorilerim bölümüne tıklanma kontrolü
        test_7_clickMyFavorite();
        //Favorilerimdeki ürünlerin ayrıntılı sayfasına geçiş kontrolü
        test_8_clickedFavoriteOpenDetailPage();
        //Favorilerimde daha önce eklenmiş ürünün onaylanma kontrolü
        test_9_clickedFavoriteConfirmation();
        //Favorilerden  ürünün silinme kontrolü
        test_10_deleteSelectedProduct();
        //Silinen ürünün favorilerim listesinde bulunmadığının kontrolü
        test_11_checkDeletedFavorite();

        System.out.println("Tüm Testler Başarılı...");
    }
    //Anasayfa açılış kontrolü
    @Test
    public void test_1_WebSiteControl(){

        driver.get(webSiteUrl);
        waitForLoading();
        Assert.assertTrue(driver.getTitle().equals("n11.com - Alışverişin Uğurlu Adresi"));
        System.out.println("*** Web Sitesi Yüklendi *** ");
    }
    //Kullanıcı giriş sayfası açılma kontrolü
    @Test
    public void test_2_loginPageReadyControl(){
        System.out.println("***Kullanıcı Giriş Sayfası  yükleniyor.***");
        System.out.println(" >>> \n");

        findByClassName("btnSignIn").click();
        wait.until(elementClickableById("loginButton"));
        Assert.assertTrue(findById("loginButton").getText().equals("Üye Girişi"));

        System.out.println("*** Kullanıcı Giriş Sayfası  Yüklendi. Kullanıcı girişi için hazır.*** ");

    }
    //Kullanıcı girişi kontrolü
    @Test
    public void test_3_loginControl(){
        System.out.println("*** Kullanıcı Şuan Giriş Yapıyor...***");
        System.out.println(" >>> \n");
        //http://www.n11.com sayfasınd üye olunuz ve mail,şifrenizi burda belirttiniz.
        findById("email").sendKeys("s.yalcin1018@gmail.com");
        findById("password").sendKeys("571sevgili632");
        findById("loginButton").click();
        waitForLoading();

        System.out.println("*** Kullanıcı Girişi Başarılı.***");


    }
    //samsung için arama sonucu kontrolü
    @Test
    public void test_4_searchControl(){
        wait.until(elementClickableById("searchData"));
        findById("searchData").sendKeys("samsung");
        findByClassName("searchBtn").click();
        System.out.println("*** Samsung için sonuçlar  aranıyor.*** ");
        System.out.println(" >>> \n");
        wait.until(elementClickableByClassName("pagination"));

        System.out.println("*** Samsung için sonuçlar  bulundu.*** ");
    }
    //ikinci sayfaya tıklanma ve 2. sayfanın açılma kontrolü
    @Test
    public void test_5_clickPageAndPageTwoOpenedControl(){
        findByXpad("//*[@class='pagination']/a[2]").click();
        System.out.println("*** Ürünlerin 2. Sayfası Yükleniyor .*** ");
        System.out.println(" >>> \n");

        waitForLoading();
        System.out.println("*** Ürünlerin 2. Sayfası  Açıldı.*** ");
    }

    //listede 3. ürünün favorilere eklenme kontrolü
    @Test
    public void test_6_addThirdProductFavorite(){
        System.out.println("*** Ürün Listesinde Bulunan 3. Ürün Favoriler Listesine Ekleniyor.*** ");
        System.out.println(" >>> \n");

        //wait.until(elementClickableByXpad(".//*[@id='p-171080208']/div[2]/span[2]"));
        wait.until(elementClickableByXpad(".//*[@id='view']/ul/li[3]/div/div[2]/span"));
        FavoriteProduct=getElementTextByXpad("//li[3]/div/div/a/h3");
        findByXpad(".//*[@id='view']/ul/li[3]/div/div[2]/span").click();
        System.out.println("*** Favoriye  "+FavoriteProduct+"  Ürünü Eklendi.*** ");
    }

    //Favorilerim bölümüne tıklanma kontrolü
    @Test
    public void test_7_clickMyFavorite(){
        System.out.println("*** Favoriler Sayfası Yükleniyor*** ");
        System.out.println(" >>> \n");

        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].click();", findByXpad("//*[@class='myAccountMenu hOpenMenu']/div[1]/a[2]"));
        waitForLoading();
        System.out.println("*** Favoriler Sayfası Yüklendi*** ");
    }

    //Favorilerimdeki ürünlerin ayrıntılı sayfasına geçiş kontrolü
    @Test
    public void test_8_clickedFavoriteOpenDetailPage(){
        wait.until(elementClickableByXpad("//ul[@class='listItemProductList']/li/a"));
        findByXpad("//ul[@class='listItemProductList']/li/a").click();

        System.out.println("*** Favorilerimdeki, ürünlerin  ayrıntılı bilgi  sayfası açıldı ***");


    }
    //Favorilerimde daha önce eklenmiş ürünün onaylanma kontrolü
    @Test
    public void test_9_clickedFavoriteConfirmation(){
        List<WebElement> productTitles= findListByXpad("//*[@class='group listingGroup wishListGroup']/div[2]/ul/li");
        for (WebElement productTitle : productTitles) {

            String  watchesProduct=productTitle.getText();

            //Gelen getText() string değerini ayrıştırdık.
            String split[] = watchesProduct.split("\n");
            String splitWatchesProduct =split[0];

            if (splitWatchesProduct.equals(FavoriteProduct)) {
                System.out.println("*** Favoriye Eklenen"+splitWatchesProduct+" Ürünü  Onaylandı. *** \n");

            }
        }
    }
    //Favorilerden  ürünün silinme kontrolü
    @Test
    public void test_10_deleteSelectedProduct(){
        System.out.println("*** Favorilerden "+FavoriteProduct+"ürün siliniyor ***");
        findByXpad(".//*[@id='view']/ul/li/div/div[3]/span").click();
        waitForLoading();
        System.out.println("*** Favorilerden "+FavoriteProduct+"   ürün silindi ***");
    }

    //Silinen ürünün favorilerim listesinde bulunmadığının kontrolü
    @Test
    public void test_11_checkDeletedFavorite(){
        String splitWatchesProduct="";
        List<WebElement> productTitles= findListByXpad("//*[@class='group listingGroup wishListGroup']/div[2]/ul/li");
        for (WebElement productTitle : productTitles) {

            String watchesProduct=productTitle.getText();

            //Gelen getText() string değerini ayrıştırdık.
            String split[] = watchesProduct.split("\n");
            splitWatchesProduct =split[0];
        }

        System.out.println("Favorilerim Sayfasında "+splitWatchesProduct+"   adlı  ürün artık bulunmuyor ...");
    }



}
